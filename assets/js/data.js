export const animeProp= [
    {
      category : 'Trending Now',
      title : 'The Seven Deadly Sins: Wrath of the Gods',
      img: 'assets/img/trend-1.jpg',
    },{
        title : 'Gintama Movie 2: Kanketsu-hen - Yorozuya yo Eien',
        img: 'assets/img/trend-2.jpg',
    },
    {
        title : 'Shingeki no Kyojin Season 3 Part 2',
        img: 'assets/img/trend-3.jpg',
    },
    {
        title : 'Fullmetal Alchemist: Brotherhood',
        img: 'assets/img/trend-4.jpg',
    },
    {
        title : 'Shiratorizawa Gakuen Koukou',
        img: 'assets/img/trend-5.jpg',
    },
    {
       title : 'Code Geass: Hangyaku no Lelouch R2',
        img: 'assets/img/trend-6.jpg',
    },
    
]
export const popularProp= [
    {
      title : 'The Seven Deadly Sins: Wrath of the Gods',
      img: 'assets/img/popular-1.jpg',
      category : "POPULAR SHOWS"
    },{
        title : 'Gintama Movie 2: Kanketsu-hen - Yorozuya yo Eien',
        img: 'assets/img/popular-2.jpg'
    },
    {
        title : 'Shingeki no Kyojin Season 3 Part 2',
        img: 'assets/img/popular-3.jpg'
    },
    {
        title : 'Fullmetal Alchemist: Brotherhood',
        img: 'assets/img/popular-4.jpg'
    },
    {
        title : 'Shiratorizawa Gakuen Koukou',
        img: 'assets/img/popular-5.jpg'
    },
    {
       title : 'Code Geass: Hangyaku no Lelouch R2',
        img: 'assets/img/popular-6.jpg'
    }
]
export const recentProp= [
    {
      title : 'The Seven Deadly Sins: Wrath of the Gods',
      img: 'assets/img/recent-1.jpg',
      category : "RECENTLY ADDED SHOWS"
    },{
        title : 'Gintama Movie 2: Kanketsu-hen - Yorozuya yo Eien',
        img: 'assets/img/recent-2.jpg'
    },
    {
        title : 'Shingeki no Kyojin Season 3 Part 2',
        img: 'assets/img/recent-3.jpg'
    },
    {
        title : 'Fullmetal Alchemist: Brotherhood',
        img: 'assets/img/recent-4.jpg'
    },
    {
        title : 'Shiratorizawa Gakuen Koukou',
        img: 'assets/img/recent-5.jpg'
    },
    {
       title : 'Code Geass: Hangyaku no Lelouch R2',
        img: 'assets/img/recent-6.jpg'
    }
]
export const liveProp= [
    {
      title : 'The Seven Deadly Sins: Wrath of the Gods',
      img: 'assets/img/live-1.jpg',
      category : "LIVE ACTION"
    },{
        title : 'Gintama Movie 2: Kanketsu-hen - Yorozuya yo Eien',
        img: 'assets/img/live-2.jpg'
    },
    {
        title : 'Shingeki no Kyojin Season 3 Part 2',
        img: 'assets/img/live-3.jpg'
    },
    {
        title : 'Fullmetal Alchemist: Brotherhood',
        img: 'assets/img/live-4.jpg'
    },
    {
        title : 'Shiratorizawa Gakuen Koukou',
        img: 'assets/img/live-5.jpg'
    },
    {
       title : 'Code Geass: Hangyaku no Lelouch R2',
        img: 'assets/img/live-6.jpg'
    }
]

export const topviewProp = [
    {
        title : " Boruto: Naruto next generations",
    },
    {
        title : "The Seven Deadly Sins: Wrath of",
    },
    {
        title : "Boruto: Naruto next generations",
    },
    {
        title : "Sword art online alicization war of underworld"
    },
    {
        title : "Fate/stay night: Heaven's Feel I. presage flower"
    }
]