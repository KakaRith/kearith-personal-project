const trendingCards = document.querySelector('.trendingCard')
const popularCards = document.querySelector('.popularCard')
const recentCards = document.querySelector('.recentCard')
const liveCards = document.querySelector('.liveCard')
const topViewCards = document.querySelector('.topviewSection')
import { animeProp , liveProp , popularProp , recentProp , topviewProp} from "./data.js"

const randomView = () => {
  const randomNum = Math.random();
  let view;
  if (randomNum < 0.7) { // 70% chance
    view = Math.floor(Math.random() * 1000);
  }else { // 30% chance
    view = 1000 + Math.floor(Math.random() * 9000); 
  }
  return view >= 1000 ? (view / 1000).toFixed(1) + 'M' : view + 'K';
}

const randomRating = () => {
  const random = Math.random();
  if (random < 0.3) return Math.floor(Math.random() * 7) + 1;
  if (random < 0.6) return Math.floor(Math.random() * 10) + 1;
  return Math.floor(Math.random() * (random < 0.9 ? 5 : 10)) + 1;
};

const randomComment = () => Math.floor(Math.random() * 100) + 1;


const animeCardGenerator = ( htmldoc , animeData ) => {
    let HTML = '';
    let category = `<div class="d-flex pb-4 justify-content-between">
              <h3 class="oswald section-title animeTitle">${animeData[0].category}</h3>
              <p class="viewmore animeTitle" style="cursor: pointer;"> View more
                <i class="fa-solid fa-arrow-right"></i></p>
            </div>`
    animeData.forEach(function(anime){
     let cards = `
     <div class="col-6 col-md-4 animeCard-wrapper  pt-3">
     <a class="linktodetail" href="pages/detail.html">
       <div class="animeCard ">
        <div class="card-view" style="font-size: 13px; font-weight: 300;"><i class="fa-solid fa-eye"></i> 
        ${randomView()}</div>
          <p class="card-rating">
          ${randomRating()}/10
          </p>
          <p class="card-comment" style="font-size: 13px; font-weight: 300;"><i class="fa-regular fa-comment"></i> ${randomComment()}
          </p>
        <img class="w-100" src="${anime.img}" alt="">
      </div>
      <div class="animeTag d-flex pt-2">
        <p class="ms-1">Action</p>
        <p class="ms-1">Movie</p>
      </div>
      <div>
        <p class="animeTitle">${anime.title}</p>
      </div>
    </a>
  </div>`
  HTML += cards;
})
  return htmldoc.innerHTML = animeData[0].category ? category + HTML : HTML;
}

const topViewCardGenerator = ( htmldoc , animeData ) => {
  let HTML = '';
  animeData.forEach(function(anime , index){
   let cards = ` <div
              class="col-lg-12 col-sm-12 col-md-6 side-card pt-4 position-relative">
              <div class="side-rating">
               ${randomRating()}/10
              </div>
              <div class="side-card-title text-center">
               ${anime.title}
              </div>
              <div class="d-flex justify-content-end">
              <span class="side-card-view">
                <i class="fa-solid fa-eye"></i> ${randomView()}
              </span></div>
              <img class="w-100" src="assets/img/tv-${index+1}.jpg" alt>
            </div>
            `
     HTML += cards;
  })
  return htmldoc.innerHTML = HTML;
}

topViewCardGenerator(topViewCards, topviewProp)
animeCardGenerator(trendingCards, animeProp)
animeCardGenerator(popularCards, popularProp)
animeCardGenerator(recentCards, recentProp)
animeCardGenerator(liveCards, liveProp)


